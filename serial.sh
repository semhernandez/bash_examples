stty -F /dev/ttyUSB0 raw
stty -F /dev/ttyUSB0 -echo
data=''
while read -rs -n 1 c #&& [[ $c != $'\x00' ]]
do
    data=$data$c # Replace this with code to handle the characters read
done < /dev/ttyUSB0
echo $data
