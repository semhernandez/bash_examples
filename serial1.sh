#!/bin/bash

stty -F /dev/ttyUSB0 raw
stty -F /dev/ttyUSB0 -echo

echo "hola" > /dev/ttyUSB0
sleep 1

while read -rs -t 2 line 
do
    # $line is the line read, do something with it
    echo $line
done < /dev/ttyUSB0
