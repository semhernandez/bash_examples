#!/bin/bash

pwd = $(pwd)

VAR=1

while ls=$(ls -lh) && [ $VAR != 0 ]
do
    echo "hola $ls"
    read VAR
done

VAR=1
until false || [ $VAR == 0 ]
do
    echo "hola"
    read VAR
done

for i in `ls`
do
    echo $i
done

for (( i=0 ; i <=10 ; i++ ))
do
    echo $i
done
