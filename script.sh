#!/bin/bash

echo "Seleccione un menu: 1 o 2 "
read VALOR
if [[ $VALOR == 1 ]]; then
    echo "******************************"
    echo "* 1) Seleccionaste menu coko *"
    echo "******************************"
elif [[ $VALOR == 2 ]]; then
    echo   "********************************"
    printf "* 2) Seleccionaste menu Yeison *\n"
    echo   "********************************"
else
    echo "************************************"
    echo "* $VALOR no esta dentro de las opciones *"
    echo "************************************"
fi
##############################################################
# Los parametros que le pasamos al scrip se almacenan en las #
# Variables numericas ejem ./scrip.sh parametro1 parametro2  #
##############################################################
echo "Parametros pasados al progrma $1 $2 $3"
shift

echo "Parametros pasados al progrma despues de shift $1 $2 $3"

##
#  mas Variables del script
echo $0
echo $#
echo $*
echo $@
echo $_
echo $$
echo $?
