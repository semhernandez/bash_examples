#!/bin/bash

stty -F $1 raw 115200
stty -F $1 -echo
echo $1 $2
exec 3<> $1
sleep 2
echo hola >&3
sleep 1

while read -rs -t 2 line 
do
    # $line is the line read, do something with it
    echo $line
done <&3 > $2

if [ ${#3} != 0 ]; then
    echo $3
else
    echo NADA
fi


exec 3>&-

#cat $2
