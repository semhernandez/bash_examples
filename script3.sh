#!/bin/bash

echo "Digite un numero:"
read NUMERO

if [ $NUMERO -eq 1 ]; then
   echo "Seleccionaste el $NUMERO"
else
    echo "Seleccionaste un numero"
fi

echo "Nombre del script:"
read SCRIPT

echo "buscando el archivo $SCRIPT:"

if [ -f $SCRIPT ]; then
    cat $SCRIPT
elif [ $SCRIPT == "/tmp/temp.txt" ]; then
    echo "creando el archivo $SCRIPT"
    touch $SCRIPT
    if [ -f $SCRIPT ]; then
	       echo "Archivo creado exitosamente"
    fi
else
    echo "el archivo $SCRIPT no existe"
fi
